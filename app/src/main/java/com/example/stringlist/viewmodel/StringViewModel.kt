package com.example.stringlist.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.stringlist.model.StringRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class StringViewModel : ViewModel() {
    private val repo = StringRepo
    private var tempList = mutableListOf<String>()

    private val _strings = MutableLiveData<List<String>>()
    val strings: LiveData<List<String>> get() = _strings

    fun getStrings() = viewModelScope.launch {
        val itemList = tempList
        _strings.value = itemList
    }

    fun addString(string: String) {
        tempList.add(string)
    }
}
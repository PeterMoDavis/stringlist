package com.example.stringlist.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.stringlist.R
import com.example.stringlist.databinding.FragmentStringSeparateBinding

class StringSeparateFragment : Fragment() {
    private var _binding : FragmentStringSeparateBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<StringSeparateFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentStringSeparateBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    binding.separatedGrid.apply {
        val separatedString = args.getString.toString().split("")
        layoutManager = LinearLayoutManager(context)
        adapter = StringSeparateAdapter().apply {
            addStringSeparated(separatedString)
        }
    }

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
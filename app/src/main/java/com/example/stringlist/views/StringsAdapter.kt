package com.example.stringlist.views

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.stringlist.databinding.ItemStringBinding

class StringsAdapter : RecyclerView.Adapter<StringsAdapter.StringsViewHolder>() {

    private var strings = mutableListOf<String>()
    private var adapterLister: ((String) -> Unit)? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): StringsViewHolder {
        val binding = ItemStringBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return StringsViewHolder(binding)
    }

    override fun onBindViewHolder(stringsViewHolder: StringsViewHolder, position: Int) {
        val string = strings[position]
        stringsViewHolder.loadString(string)
        stringsViewHolder.setClickListener {
            adapterLister?.invoke(string)
        }
    }

    override fun getItemCount(): Int {
        return strings.size
    }

    fun addStrings(string: MutableList<String>) {
        this.strings = string.toMutableList()
        notifyDataSetChanged()
    }

    fun addItemClickListener(function: (String)-> Unit){
        adapterLister = function
    }

    class StringsViewHolder(
        private val binding: ItemStringBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun loadString(string: String) {
            binding.itemString.text = string
        }
        fun setClickListener(clickListener: View.OnClickListener?) {
            binding.itemString.setOnClickListener(clickListener)
        }
    }
}
package com.example.stringlist.views

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.stringlist.databinding.ItemSeparatedBinding

class StringSeparateAdapter :
    RecyclerView.Adapter<StringSeparateAdapter.StringSeparateViewHolder>() {

    private var stringList = mutableListOf<String>()


    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ): StringSeparateViewHolder {
        val binding = ItemSeparatedBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return StringSeparateViewHolder(binding)
    }

    override fun onBindViewHolder(stringSeparatedViewHolder: StringSeparateViewHolder, position: Int) {
        val stringSeparated = stringList[position]
        stringSeparatedViewHolder.loadString(stringSeparated)
    }

    override fun getItemCount(): Int {
        return stringList.size
    }

    fun addStringSeparated(string: List<String>){
        this.stringList = string.toMutableList()
        notifyDataSetChanged()
    }

    class StringSeparateViewHolder(
        private val binding: ItemSeparatedBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadString(string: String) {
            binding.separatedItem.text = string
        }

    }
}

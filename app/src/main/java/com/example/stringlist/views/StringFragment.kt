package com.example.stringlist.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.stringlist.R
import com.example.stringlist.databinding.FragmentStringBinding
import com.example.stringlist.viewmodel.StringViewModel

class StringFragment : Fragment() {
    private var _binding: FragmentStringBinding? = null
    private val binding get() = _binding!!
    private val stringViewModel by viewModels<StringViewModel> ()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    )=FragmentStringBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnSubmitString.setOnClickListener {
            val string = binding.teString.text.toString()
            stringViewModel.addString(string)

            binding.stringGrid.layoutManager = GridLayoutManager(this.context, 3)
            binding.stringGrid.adapter = StringsAdapter().apply {
                stringViewModel.getStrings()
                stringViewModel.strings.observe(viewLifecycleOwner){
                    addStrings(it as MutableList<String>)
                    addItemClickListener { string : String ->
                        findNavController().navigate(StringFragmentDirections.actionStringFragmentToStringSeparateFragment(string))
                    }
                }
            }
        }
    }

}
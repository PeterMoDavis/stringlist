package com.example.stringlist.model

interface StringApi {
    suspend fun getStrings(): List<String>
}
package com.example.stringlist.model

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

object StringRepo {
    private val stringApi = object : StringApi {
        override suspend fun getStrings(): List<String> {
            return listOf()
        }
    }

    suspend fun getString(): List<String> = withContext(Dispatchers.IO) {

        stringApi.getStrings()
    }
}